package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.Ville;
import java.util.Optional;

@Repository
public interface VilleRepository extends JpaRepository<Ville, Integer> {
	@Query("select v from Ville v where v.name like %:name% ")
	 public List<Ville> findVilleByName(@Param("name") String name);
	 Optional<Ville> findById(int id);
}
