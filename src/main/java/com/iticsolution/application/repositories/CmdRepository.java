package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.Cmd;

@Repository
public interface CmdRepository  extends JpaRepository<Cmd, Long>{
	public List<Cmd> findCMDBynumCMD(@Param("word") String numCMD);
}
