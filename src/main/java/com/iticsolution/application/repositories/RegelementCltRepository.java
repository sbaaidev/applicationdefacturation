package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.RegelementCLT;

@Repository
public interface RegelementCltRepository extends JpaRepository<RegelementCLT, Long> {
	 public List<RegelementCLT> findRegelemtByNumFactureClt(@Param("word") String numFactureClt);

}
