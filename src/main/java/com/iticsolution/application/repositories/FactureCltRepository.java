package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.FactureClt;

@Repository
public interface FactureCltRepository extends JpaRepository<FactureClt, Long> {
	public List<FactureClt> findFactureBynumFactureClt(@Param("word") String numFactureClt);

}
