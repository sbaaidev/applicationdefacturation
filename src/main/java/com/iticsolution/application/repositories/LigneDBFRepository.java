package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iticsolution.application.entities.Ligne_DBF;

@Repository
public interface LigneDBFRepository  extends JpaRepository<Ligne_DBF,Long>{
	 public List<Ligne_DBF> findLingneDBFById(@Param("id") int id);
}
