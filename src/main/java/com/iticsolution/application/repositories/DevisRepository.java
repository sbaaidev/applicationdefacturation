package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.Devis;

@Repository
public interface DevisRepository extends JpaRepository<Devis, Long>{
	public List<Devis> findDevisBynumDevis(@Param("word") String numDevis);

}
