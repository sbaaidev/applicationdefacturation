package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.RegelementFrss;

@Repository
public interface RegelementFrssRepository  extends JpaRepository<RegelementFrss, Long>{
	 public List<RegelementFrss> findRegelemtByNumFactureFrss(@Param("word") String numFactureFrss);
}
