package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.ProduitClient;

@Repository
public interface ProduitClientRepository  extends JpaRepository<ProduitClient, Long>{
	 public List<ProduitClient> findLingneDBFByDesignation(@Param("word") String designation);
}
