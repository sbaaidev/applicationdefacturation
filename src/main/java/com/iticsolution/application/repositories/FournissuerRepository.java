package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.Fournisseur;

@Repository
public interface FournissuerRepository  extends JpaRepository<Fournisseur, Long>{
	public List<Fournisseur> findFournisseurBynomComplet(@Param("word") String nomComplet);

}
