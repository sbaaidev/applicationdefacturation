package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.FactureFrss;


@Repository
public interface FactureFrssRepository  extends JpaRepository<FactureFrss, Long>{
	public List<FactureFrss> findFactureFrssByNumFact(@Param("word") String numFact);

}
