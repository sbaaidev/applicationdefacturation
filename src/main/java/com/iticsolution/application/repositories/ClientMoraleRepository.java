package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.ClientMorale;

@Repository
public interface ClientMoraleRepository extends JpaRepository<ClientMorale, Long> {
	@Query("select cm from ClientMorale cm where cm.nom like %:nom% ")
	 public List<ClientMorale> findClientMoraleByNom(@Param("nom") String nom);


}
