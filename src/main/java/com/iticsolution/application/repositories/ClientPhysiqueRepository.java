package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.ClientPhysique;

@Repository
public interface ClientPhysiqueRepository extends JpaRepository<ClientPhysique, Long> {
	 public List<ClientPhysique> findClientPhysiqueByNom(@Param("nom") String nom);
}
