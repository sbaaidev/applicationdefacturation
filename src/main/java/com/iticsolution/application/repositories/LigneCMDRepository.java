package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.LingneCommande;

@Repository
public interface LigneCMDRepository  extends JpaRepository<LingneCommande, Long>{
	 public List<LingneCommande> findLingneDBFById(@Param("id") int id);

}
