package com.iticsolution.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.iticsolution.application.entities.BonService;


@Repository
public interface BonServiceRepository  extends JpaRepository<BonService, Long>{
	public List<BonService> findBonServiceBynumBS(@Param("word") String numBS);

}
