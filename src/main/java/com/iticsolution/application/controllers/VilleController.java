package com.iticsolution.application.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iticsolution.application.entities.Ville;
import com.iticsolution.application.services.VilleService;

@RestController
@CrossOrigin("*") 
public class VilleController {


	@Autowired
	private VilleService villeService ;
	
	
	@RequestMapping(value="/villes", method = RequestMethod.GET)
	public List<Ville> getVillesFromController(){
		return villeService.getVilles();
	}
	
	
	@RequestMapping(value = "/ville" , method = RequestMethod.GET)
	public Ville getVilleFromController(@RequestParam("id") String id){
		return villeService.getVilleById(Integer.parseInt(id));
	}
	
	
	@RequestMapping (value = "/ville" , method = RequestMethod.POST)
	public void addVille(@RequestBody Ville value) {
		villeService.addVille(value);
	}
	
	
	@RequestMapping(value = "/ville" , method = RequestMethod.DELETE )
	public void deleteVille(@RequestParam("id") String id) {
		villeService.deleteVille(Integer.parseInt(id));
	}
	
	
	@RequestMapping(value = "/ville" , method = RequestMethod.PUT )
	public void updateVille(@RequestBody Ville ville) {
		villeService.updateVille(ville);
	}
	
	
	@RequestMapping(value = "/villesearch" , method = RequestMethod.GET)
	public List<Ville> getVillesSearch(@RequestParam("name") String name){
		return villeService.getVilleByName((name));
	}
}
