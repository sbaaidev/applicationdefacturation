package com.iticsolution.application.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue(value="CM")
public class ClientMorale extends Client {
	private String FormeJuridique ;
	private String TVA ;
	private String faitgeneratuer ;
	private int IFF;
	private int IP;
	private int NumRC;
	private int numCSS;
	private int TP;
	private String tribunal ;
	private int ICE;
	private float capital ; 
	private String Banque;
	private int numRIB;
	private int codepostal;
	private int tel;
	private int fax;
	private String email;
	private String nomContact ;
	private String prenomContact;
	private int GMS ;
	private String gerant ;
	private String siteweb;
		
}
