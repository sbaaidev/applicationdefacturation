package com.iticsolution.application.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@DiscriminatorValue(value="BS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BonService  extends Devis{
	
	@Column(unique = true)
	private int numBS;
	private Date BS;
}
