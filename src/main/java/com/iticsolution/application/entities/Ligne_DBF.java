package com.iticsolution.application.entities;



import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name ="Ligne_DBF")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ligne_DBF {
	@Id @GeneratedValue
	private Long id;
	private int quantite ;
	private float prixUnit; 
	private float MontantHT;
	private float tva;
	@ManyToOne
	private Devis idDevis;
	@ManyToOne
	private ProduitClient produitclient ;
	private float HT;
	private float totalHT;
	private float totalTVA;
	private float totalTTC;
	

}
