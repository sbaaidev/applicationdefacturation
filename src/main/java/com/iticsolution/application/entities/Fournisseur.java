package com.iticsolution.application.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="Fournisseur")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fournisseur {
	@Id @GeneratedValue
	private Long id ;
	private String nomComplet;
	private int tel ;
	private String email;
	private String ICE;
	private String IF;
	private String TP;
	private String Adresse;
	@OneToMany(mappedBy = "idFourn")
	private List<Cmd> cmd ;
}
