package com.iticsolution.application.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name ="LingneCommande")
@Entity
@Data
@NoArgsConstructor
public class LingneCommande {
	@Id @GeneratedValue
	private Long id;
	private int qte;
	private float prixUnit;
	private float monntantHT;
	private float TVA;
	@ManyToOne
	private ProduitFrss idProduitfrss;
	private float totalHT;
	private float totalTVA;
	private float totalTTC;
}
