package com.iticsolution.application.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FactureFrss extends Cmd {
	@Column(unique = true)
	private String numFact;
	private Date dateFact;
	private String modeRegelement;
	private String numCheque;
	private String numEffet;

}
