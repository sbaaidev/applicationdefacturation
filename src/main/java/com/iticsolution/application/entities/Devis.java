package com.iticsolution.application.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "devis")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_DEVIS", discriminatorType = DiscriminatorType.STRING)
public class Devis implements Serializable {
	@Id @GeneratedValue
	private Long id;
	@Column (unique = true)
	private String numDevis ;
	private Date dateDevis;
	@ManyToOne
	private Client idClient ;
	@OneToMany(mappedBy = "idDevis")
	private List<Ligne_DBF> lignsDBF;
	/*
	 * @OneToMany(mappedBy = "Devis") private java.util.List<RegelementHT>
	 * regelementHTs;
	 */
}
