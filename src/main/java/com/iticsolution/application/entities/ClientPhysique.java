package com.iticsolution.application.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@DiscriminatorValue(value="CP")
@AllArgsConstructor
@Data
public class ClientPhysique extends Client {
	private String prenom;
	private String mobile;
	private String cin;
	
}
