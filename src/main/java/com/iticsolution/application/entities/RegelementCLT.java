package com.iticsolution.application.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Table(name="RegelementCLT")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegelementCLT {
	@Id @GeneratedValue
	private Long id;
	@ManyToOne
	private FactureClt numFactureClt;
	private Date dateRegelement;
	private float montantTTC;
	private float solde;
	private float credit;
	private float debit ; 
	private float totalDebit;
	private float totalSolde;
	private String modeRegelement;
	private String numCheque;
	private String numEffet;
	
	
}
