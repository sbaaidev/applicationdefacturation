package com.iticsolution.application.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name ="ProduitFrss")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduitFrss {
	@Id @GeneratedValue
	private Long idProduitfrss;
	private String designation;
	private float prixUnit;
	private float TVA;
	@OneToMany(mappedBy = "idProduitfrss")
	private List<LingneCommande> ligneCommande ;
}
