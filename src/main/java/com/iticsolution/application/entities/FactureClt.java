package com.iticsolution.application.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Immutable;
import org.hibernate.type.TrueFalseType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue(value="FA")
public class FactureClt extends  Devis{
	@Column(unique = true)
	private String numFactureClt ;
	private Date Facture ;
	private String modeReglement ;
	private float numCheque ;
	private float numEffet;
	
	

}
