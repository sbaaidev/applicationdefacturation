package com.iticsolution.application.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "produitclient")
@Entity
@Data
@NoArgsConstructor
public class ProduitClient {
	@Id @GeneratedValue
	private Long idProduit;
	private String designation;
	private float prixUnit;
	private float tva;
	@OneToMany(mappedBy = "produitclient")
	private List<Ligne_DBF> ligne_DBF ;
}
