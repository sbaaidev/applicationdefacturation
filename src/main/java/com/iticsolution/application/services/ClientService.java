package com.iticsolution.application.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iticsolution.application.entities.Client;
import com.iticsolution.application.repositories.ClientRepository;



@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	public List<Client> getClient(String word){
		
		return clientRepository.findClientByNom(word);
	}
	  
}
