package com.iticsolution.application.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.iticsolution.application.entities.Ville;
import com.iticsolution.application.repositories.VilleRepository;

@Service
public class VilleService {

	@Autowired
	private VilleRepository villeRepository;

	public Optional<Ville> findById(int id) {
		return villeRepository.findById(id);
	}

	public void addVille(Ville ville) {
		villeRepository.save(ville);
	}

	public List<Ville> getVilles() {

		return (List<Ville>) villeRepository.findAll();
	}

	public void deleteVille(int idVille) {
		villeRepository.deleteById(idVille);
	}
	
	public void updateVille(Ville ville) {
		
		Ville upatedVille = getVilleById(ville.getId());
		upatedVille.setName(ville.getName());
		villeRepository.save(upatedVille);
	}

	public Ville getVilleById(int id) {

		return getVilleById(id);
	}
	
	public List<Ville> getVilleByName(String villeName) {
		return villeRepository.findVilleByName(villeName);
	}
}
